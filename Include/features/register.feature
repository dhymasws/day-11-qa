#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@register
Feature: register

  @register-success
  Scenario: 
    Given I open app for register
    When i click create account text
    When i fill "mamang", "mamang@gmail.com", "qwerty123", and "qwerty123"
    And i click register button
    Then register success
    
   
   @register-failed
   Scenario:
   	Given I open app to regist
   	When i create account
   	When i input "mamang", "mamang@gmail.com", "qwerty123", and "qwerty"
   	And i tap register button
   	Then register failed because password not matches