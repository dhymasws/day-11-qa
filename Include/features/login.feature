#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@login
Feature: login

  @login_invalid
  Scenario: 
    Given I open app
    When i type "iorang@gmail.com" and "ipassword"
    And i click login button 
    Then I verify the invaild message
    
  
  @login_email_format_invalid
  Scenario:
  	Given I open app for login
  	When i input "thisemail" and "thispassword"
  	And i click the login button
  	Then I verify the invalid email message