package auth
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class register {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("I open app for register")
	public void i_open_app() {
		WebUI.callTestCase(findTestCase('pages/auth/i_open_app'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Given("I open app to regist")
	public void i_open_apl() {
		WebUI.callTestCase(findTestCase('pages/auth/i_open_app'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("i click create account text")
	public void i_click_create_acc() {
		WebUI.callTestCase(findTestCase('pages/auth/i_click_reg_text'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("i create account")
	public void i_create_acc() {
		WebUI.callTestCase(findTestCase('pages/auth/i_click_reg_text'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("i fill {string}, {string}, {string}, and {string}")
	public void i_type_and(String string, String string2, String string3, String string4) {
		WebUI.callTestCase(findTestCase('pages/auth/i_type_name_email_pass_cnfpass_reg'), [('name'):string, ('email'):string2, ('pass'):string3, ('cnfpass'):string4], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("i input {string}, {string}, {string}, and {string}")
	public void i_input(String string, String string2, String string3, String string4) {
		WebUI.callTestCase(findTestCase('pages/auth/i_type_name_email_pass_cnfpass_reg'), [('name'):string, ('email'):string2, ('pass'):string3, ('cnfpass'):string4], FailureHandling.STOP_ON_FAILURE)
	}

	@When("i click register button")
	public void i_click_btn_reg() {
		WebUI.callTestCase(findTestCase('pages/auth/i_click_btn_reg'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("i tap register button")
	public void i_register() {
		WebUI.callTestCase(findTestCase('pages/auth/i_click_btn_reg'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("register success")
	def register_success() {
	}
	
	@Then("register failed because password not matches")
	def register_failed() {
		
	}
}