package auth
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class login {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("I open app")
	public void i_open_app() {
		WebUI.callTestCase(findTestCase('pages/auth/i_open_app'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Given("I open app for login")
	public void i_open_for_login() {
		WebUI.callTestCase(findTestCase('pages/auth/i_open_app'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("i type {string} and {string}")
	public void i_type_and(String string, String string2) {
		WebUI.callTestCase(findTestCase('pages/auth/i_type_name_pass'), [('name'):string, ('pass'):string2], FailureHandling.STOP_ON_FAILURE)
	}

	@When("i input {string} and {string}")
	public void i_input(String string, String string2) {
		WebUI.callTestCase(findTestCase('pages/auth/i_type_name_pass'), [('name'):string, ('pass'):string2], FailureHandling.STOP_ON_FAILURE)
	}

	@When("i click login button")
	public void i_click_login() {
		WebUI.callTestCase(findTestCase('pages/auth/i_click_btn_login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("i click the login button")
	public void i_click_the_loggin() {
		WebUI.callTestCase(findTestCase('pages/auth/i_click_btn_login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify the invaild message")
	public void i_verify_the_invaild_message() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/auth/i_verify_the_invaild_message'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify the invalid email message")
	public void i_verivy_invalid_email() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/auth/i_verify_email_error'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}